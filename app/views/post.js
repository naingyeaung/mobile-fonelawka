import React, { Component } from 'react';
import { View, Text, ScrollView, RefreshControl, TouchableOpacity } from 'react-native';
import { BACKGROUND_COLOR, API_URL, CONDITION_TYPE } from '../components/common';
import { getData } from '../services/fetch';
import { PostCard, FilterCard } from '../components/card';
import Br from '../components/br';
import PostStore from '../mobx/PostStore';
import { observer } from 'mobx-react';
import { PReg } from '../components/text';

class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      refreshing: false
    };
  }

  async componentDidMount() {
    await this.getAllPosts();
    console.log(PostStore.Posts.toJS())
  }

  async getAllPosts() {
    this.setState({ refreshing: true });
    await PostStore.load();
    this.setState({ refreshing: false });
  }

  async clearBrand() {
    this.setState({ refreshing: true });
    PostStore.updateBrandId('', '');
    await PostStore.load();
    this.setState({ refreshing: false });
  }

  render() {
    return (
      <View style={{ backgroundColor: BACKGROUND_COLOR, flex: 1 }}>
        {/* <View style={{ padding: PostStore.BrandId != '' ? 10 : 0 }}>
          {
            PostStore.BrandId != '' ? (<View style={{ flexDirection: 'row' }}>
              <FilterCard text={PostStore.BrandName} onPress={() => this.clearBrand()}></FilterCard>
            </View>) : <></>
          }
        </View> */}
        <ScrollView style={{ backgroundColor: BACKGROUND_COLOR }} refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={() => this.getAllPosts()}
          />
        }>

          <View style={{ padding: 5 }}>
            {
              PostStore.Posts.length > 0 ? (
                <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between' }}>
                  {
                    PostStore.Posts.map((post, index) =>
                      <View key={index} style={{ width: '49%' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('PostDetail', { postId: post.id })}>
                          <PostCard id={post.id} count={post.count} totalcount={post.totalcount} navigation={this.props.navigation} img={{ uri: post.picserver + post.picpath + '/' + post.picname }} price={post.price != null ? post.price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") : post.price} title={post.title} phone={post.phone} description={post.des.length > 50 ? post.des.slice(0, 50) + '...' : post.des} seller={post.name} condition={CONDITION_TYPE[post.condition]}>

                          </PostCard>
                        </TouchableOpacity>
                        <Br height={15}></Br>
                      </View>
                    )
                  }
                </View>
              ) : (<View style={{justifyContent:'center',flex:1}}>
                <PReg>No posts to show</PReg>
              </View>)
            }

          </View>
        </ScrollView>
      </View>
    );
  }
}

export default observer(Post);
