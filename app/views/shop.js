import React, { Component } from 'react';
import { View, Text, ScrollView, Image } from 'react-native';
import { PRIMARY_COLOR, API_URL } from '../components/common';
import { getData } from '../services/fetch';
import { PReg } from '../components/text';

class Shop extends Component {
    constructor(props) {
        super(props);
        const { navigation } = this.props;
        this.state = {
            shopId: navigation.getParam('shopid'),
            shop: {},
            posts: []
        };
    }

    async componentDidMount() {
        console.log(this.state.shopId);
        var response = await getData(API_URL+'basic/getCustomerDetailinfo?id='+this.state.shopId);
        if(response.ok){
            this.setState({shop:response.data.customer});
            this.setState({posts:response.data.posts})
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <ScrollView>
                    <View style={{ height: 200, justifyContent: 'center', backgroundColor: PRIMARY_COLOR, alignItems: 'center' }}>
                        <Image style={{ height: 70, width: 70 }} source={require('../assets/images/f-cover.png')}></Image>
                    </View>
                    <View style={{flexDirection:'row', padding:20}}>
                        <View style={{flex:1, justifyContent:'center'}}>
                            <PReg>{this.state.shop.name}</PReg>
                        </View>
                        <View style={{flex:1,justifyContent:'center',paddingRight:20, alignItems:'flex-end',marginTop:-50}}>
                            <Image style={{width:100,height:100}} source={{uri:this.state.shop.avatar}}></Image>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

export default Shop;
