import React, { Component } from 'react';
import { View, Text, Modal, ScrollView, Linking, Dimensions, TextInput, TouchableOpacity, Image, KeyboardAvoidingView, Platform } from 'react-native';
import { getData, postData } from '../services/fetch';
import { API_URL, BACKGROUND_COLOR, CONDITION_TYPE, PRIMARY_COLOR, API_IMG_URL, DEFAULT_ICON_SIZE, PURPOSE_TYPE } from '../components/common';
import { PostCarousel } from '../components/carousel';
import { PReg, PLg, HSm } from '../components/text';
import Br from '../components/br';
import { Button, BorderButton } from '../components/button';
import { CommentCard } from '../components/card';
import Loading from '../components/loading';
import CustomTitle from '../components/customTitle';

class PostDetail extends Component {
    constructor(props) {
        super(props);
        const { navigation } = this.props;
        const screenWidth = Math.round(Dimensions.get('window').width);
        this.state = {
            postId: navigation.getParam('postId'),
            postDetail: {},
            postPhotos: [],
            postPhone: {},
            customer: {},
            phone: '09773184989',
            sWidth: screenWidth - 10,
            comments: [],
            loading: false,
            cmtBoxVisible: false,
            replyBoxVisible: false,
            buttonVisible: true,
            commentText: '',
            replyText: '',
            currentCmt: { id: '', name: '' }
        };
    }

    async componentDidMount() {
        console.log(this.state.postId)
        this.setState({ loading: true });
        await this.getPostDetail();
        // await this.getPostPhoto();
        // await this.getPostPhone();
        await this.getComments();
        this.setState({ loading: false })
    }

    async getPostDetail() {
        var response = await getData(API_URL + 'basic/getPostDetail?id=' + this.state.postId);
        if (response.ok) {
            var post = response.data;
            this.setState({ postDetail: post.post });
            this.setState({ postPhotos: post.photos });
            this.setState({ postPhone: post.post.phone })
            this.setState({ customer: post.customer })
            var phonenumber = this.state.customer.phones != null && this.state.customer.phones.length > 0 ? this.state.customer.phones[0].phoneNumber : '';
            phonenumber = phonenumber == null || phonenumber == '' ? (this.state.customer.phones != null && this.state.customer.phones.length > 1 ? this.state.customer.phones[1].phoneNumber : '') : '';
            this.setState({ phone: phonenumber });
            console.log('post', response.data)
            // this.setState({ postDetail: post });
        }
        console.log(this.state.postDetail);
    }

    // async getPostPhoto() {
    //     var response = await getData(API_URL + 'basic/fetchPhoto/' + this.state.postId);
    //     if (response.ok) {
    //         var photos = response.data;
    //         this.setState({ postPhotos: photos })
    //         // console.log(this.state.postPhotos)
    //     }
    // }

    // async getPostPhone() {
    //     var response = await getData(API_URL + 'basic/fetchPhone/' + this.state.postDetail.phoneId);
    //     if (response.ok) {
    //         var phone = response.data;;
    //         this.setState({ postPhone: phone });
    //     }
    //     // console.log(this.state.postPhone)
    // }

    async getComments() {
        var response = await getData(API_URL + 'basic/fetchCmts/' + this.state.postId);
        if (response.ok) {
            var comments = response.data;
            this.setState({ comments: comments });
            this.state.comments.map(async (cmt, index) => {
                var result = await getData(API_URL + 'basic/fetchRps/' + cmt.id);
                if (result.ok) {
                    cmt.replies = result.data;
                    this.setState({ comments: this.state.comments });
                    // console.log(cmt.replies)
                }
            })
            // console.log(this.state.comments)
        } else {
            console.log(response.data);
        }
    }

    async giveCmt() {
        var model = {
            postId: this.state.postId,
            commentText: this.state.commentText
        }
        this.setState({ loading: true })
        var response = await postData(API_URL + 'posts/addComment', model);
        if (response.ok) {
            this.setState({ commentText: '' });
            await this.getComments();
            this.setState({ loading: false })
            this.setState({ cmtBoxVisible: false })
        }
    }

    async giveReply() {
        var model = {
            commentId: this.state.currentCmt.id,
            replyText: this.state.replyText
        }
        this.setState({ loading: true });
        var response = await postData(API_URL + 'posts/addReply', model);
        if (response.ok) {
            this.setState({ replyText: '' });
            this.setState({ replyBoxVisible: false });
            await this.getComments();
            this.setState({ loading: false });
        } else {
            console.log(response.data);
        }
    }

    call() {
        let phoneNumber = this.state.phone;
        if (Platform.OS !== 'android') {
            phoneNumber = `telprompt:${this.state.phone}`;
        }
        else {
            phoneNumber = `tel:${this.state.phone}`;
        }
        Linking.canOpenURL(phoneNumber)
            .then((supported) => {
                if (supported) {
                    return Linking.openURL(phoneNumber)
                        .catch(() => null);
                }
            });
        // Linking.openURL(phoneNumber)
    }

    openReply(id, name) {
        this.setState({ replyBoxVisible: true });
        this.setState({ cmtBoxVisible: false });
        var currentCmt = { id: id, name: name };
        this.setState({ currentCmt: currentCmt })
    }

    openCmt() {
        this.setState({ cmtBoxVisible: !this.state.cmtBoxVisible });
        this.setState({ replyBoxVisible: false });
    }


    render() {
        var phone = this.state.postPhone;
        var customer = this.state.customer != null ? this.state.customer : {};
        if (this.state.loading) {
            return (<Loading visible={this.state.loading}></Loading>)
        }
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 14 }}>
                    <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === "ios" ? "padding" : "padding"} keyboardVerticalOffset={-500}>
                        <ScrollView style={{ flex: 1 }} contentContainerStyle={{ justifyContent: 'center' }} keyboardShouldPersistTaps="handled">
                            <View style={{ height: 220, justifyContent: 'center', alignItems: 'center', width: '100%' }}>
                                <PostCarousel data={this.state.postPhotos} sliderWidth={this.state.sWidth} sliderHeight={200} itemWidth={this.state.sWidth} itemHeight={200}></PostCarousel>
                            </View>
                            <View style={{ height: 'auto', paddingTop: 20 }}>
                                <View style={{ flexDirection: 'row', backgroundColor: BACKGROUND_COLOR, height: 50 }}>
                                    <View style={{ flex: 1, borderRightWidth: 5, borderRightColor: 'white', justifyContent: 'center' }}>
                                        <PLg>{CONDITION_TYPE[this.state.postDetail.conditionType]}</PLg>
                                    </View>
                                    <View style={{ flex: 1, justifyContent: 'center', borderRightWidth: 5, borderRightColor: 'white' }}>
                                        <PLg>{phone.name}</PLg>
                                    </View>
                                    <View style={{ flex: 1, justifyContent: 'center' }}>
                                        <PLg>{this.state.postDetail.price}</PLg>
                                    </View>
                                </View>
                                <View style={{ padding: 25 }}>
                                    <HSm textAlign='left' fontWeight='bold'>{this.state.postDetail.title}</HSm>
                                    <Br height={10}></Br>
                                    <PLg textAlign='left'>{this.state.postDetail.description}</PLg>
                                    <Br height={10}></Br>
                                    <View style={{ borderColor: BACKGROUND_COLOR, borderWidth: 1 }}></View>
                                    <Br height={10}></Br>
                                    <CustomTitle text='Specifications'></CustomTitle>
                                    <Br height={10}></Br>
                                    <Spec title='BatterySize' value={phone.batterySize}></Spec>
                                    <Br height={5}></Br>
                                    <Spec title='BatteryType' value={phone.batteryType}></Spec>
                                    <Br height={5}></Br>
                                    <Spec title='CameraPixel' value={phone.cameraPixels}></Spec>
                                    <Br height={5}></Br>
                                    <Spec title='Chipset' value={phone.chipset}></Spec>
                                    <Br height={5}></Br>
                                    <Spec title='Display' value={phone.displayResolution}></Spec>
                                    <Br height={5}></Br>
                                    <Spec title='Dimension' value={phone.displaySize}></Spec>
                                    <Br height={5}></Br>
                                    <Spec title='RAM' value={phone.ramSize}></Spec>
                                    <Br height={5}></Br>
                                    <Spec title='Warranty' value={this.state.postDetail.warranty}></Spec>
                                    <Br height={10}></Br>
                                    <View style={{ borderColor: BACKGROUND_COLOR, borderWidth: 1 }}></View>
                                    <Br height={10}></Br>
                                    <CustomTitle text={PURPOSE_TYPE[this.state.postDetail.statusType] + "er Info"}></CustomTitle>
                                    <Br height={5}></Br>
                                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Shop',{shopid:this.state.postDetail.postedBy})}>
                                        <Spec title='Name' value={customer.name}></Spec>
                                    </TouchableOpacity>
                                    <Br height={5}></Br>
                                    <Spec title='Township' value={customer.township != null ? customer.township.name : ''}></Spec>
                                    <Br height={5}></Br>
                                    <Spec title='Address' value={customer.address}></Spec>
                                    <Br height={5}></Br>
                                    <Spec title='Phone' value={customer.phones != null && customer.phones.length > 0 ? customer.phones[0].phoneNumber : ''}></Spec>
                                    <Br height={5}></Br>
                                    <Spec title='Phone' value={customer.phones != null && customer.phones.length > 1 ? customer.phones[1].phoneNumber : ''}></Spec>
                                </View>
                            </View>

                            <View style={{ padding: 25 }}>
                                <PLg textAlign='left'>Comments</PLg>
                                <View style={{ borderTopColor: BACKGROUND_COLOR, borderTopWidth: 0.5, paddingTop: 10 }}>
                                    {
                                        this.state.comments.map((cmt, index) =>
                                            <View style={{ padding: 10 }} key={index}>
                                                <CommentCard onReplyPress={() => this.openReply(cmt.id, cmt.name)} img={{ uri: cmt.avatar.startsWith('/') ? API_IMG_URL + cmt.avatar : cmt.avatar }} comment={cmt.commentText} name={cmt.name}></CommentCard>
                                                {
                                                    cmt.replies != null ?
                                                        cmt.replies.map((rp, i) =>
                                                            <View style={{ padding: 20, paddingLeft: 35 }} key={i}>
                                                                <CommentCard onReplyPress={() => this.openReply(cmt.id, rp.name)} isReply={true} img={{ uri: rp.avatar.startsWith('/') ? API_IMG_URL + rp.avatar : rp.avatar }} comment={rp.replyText} name={rp.name}></CommentCard>
                                                            </View>) : <></>
                                                }
                                            </View>
                                        )
                                    }
                                </View>
                            </View>
                            {
                                this.state.cmtBoxVisible ? (
                                    <View style={{ height: 50, flexDirection: 'row', borderColor: BACKGROUND_COLOR, borderWidth: 1 }}>
                                        <View style={{ flex: 6, justifyContent: 'center' }}>
                                            <TextInput onFocus={() => this.setState({ buttonVisible: false })} onEndEditing={() => this.setState({ buttonVisible: true })} autoFocus={true} value={this.state.commentText} onChangeText={(value) => this.setState({ commentText: value })} style={{ width: '100%', height: '100%', paddingLeft: 10 }} placeholder='Comment'></TextInput>
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            <TouchableOpacity onPress={() => this.giveCmt()}>
                                                <View style={{ width: '100%', height: '100%', justifyContent: 'center' }}>
                                                    <Image style={{ width: DEFAULT_ICON_SIZE, height: DEFAULT_ICON_SIZE }} source={require('../assets/images/send.png')}></Image>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                ) : <></>
                            }
                            {
                                this.state.replyBoxVisible ? (
                                    <View style={{ height: 50, flexDirection: 'row', borderColor: BACKGROUND_COLOR, borderWidth: 1 }}>
                                        <View style={{ flex: 8, justifyContent: 'center' }}>
                                            <TextInput autoFocus={true} onFocus={() => this.setState({ buttonVisible: false })} onEndEditing={() => this.setState({ buttonVisible: true })} value={this.state.replyText} onChangeText={(value) => this.setState({ replyText: value })} style={{ width: '100%', height: '100%', paddingLeft: 10 }} placeholder={'Replying to ' + this.state.currentCmt.name}></TextInput>
                                        </View>
                                        <View style={{ flex: 2 }}>
                                            <TouchableOpacity onPress={() => this.giveReply()}>
                                                <View style={{ width: '100%', height: '100%', justifyContent: 'center' }}>
                                                    <Image style={{ width: DEFAULT_ICON_SIZE, height: DEFAULT_ICON_SIZE }} source={require('../assets/images/send.png')}></Image>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            <TouchableOpacity onPress={() => this.setState({ replyBoxVisible: false })}>
                                                <View style={{ width: '100%', height: '100%', justifyContent: 'center' }}>
                                                    <Image style={{ width: DEFAULT_ICON_SIZE, height: DEFAULT_ICON_SIZE }} source={require('../assets/images/cross.png')}></Image>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                ) : <></>
                            }
                            {/* <View style={{ height: this.state.buttonVisible == true ? 0 : 100 }}></View> */}
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>

                {
                    this.state.buttonVisible ? <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row' }}>
                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                            <BorderButton img={require('../assets/images/comment.png')} text='Comment' color={PRIMARY_COLOR} onPress={() => this.openCmt()}></BorderButton>
                        </View>
                        {/* <View style={{ width: 5 }}></View> */}
                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                            <Button text='Call' img={require('../assets/images/phone_white.png')} onPress={() => this.call()}></Button>
                        </View>
                    </View> : <></>
                }

            </View>
        );
    }
}

export default PostDetail;


class Spec extends Component {
    render() {
        return (
            <View style={{ flexDirection: 'row' }}>
                <View style={{ flex: 2 }}>
                    <PReg textAlign='left' fontWeight={'bold'}>{this.props.title}</PReg>
                </View>
                <View style={{ flex: 5 }}>
                    <PReg textAlign='left'>{this.props.value}</PReg>
                </View>
            </View>
        )
    }
}