import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { HomeHeader } from '../navigation/header';
import { ScrollView } from 'react-native-gesture-handler';
import { HSm } from '../components/text';
import { BrandCard } from '../components/card';
import { getData } from '../services/fetch';
import { API_URL, BRAND_IMG_URL } from '../components/common';
import PostStore from '../mobx/PostStore';
import Loading from '../components/loading';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            brands: [],
            loading:false
        };
    }

    async componentDidMount() {
        var response = await getData(API_URL + 'basic/getPopularBrands');
        if (response.ok) {
            this.state.brands = response.data;
            this.setState({ brands: this.state.brands });
        } else {
            console.log(response.data);
        }
        console.log(this.state.brands)
    }

    async searchWithBrand(id,name){
        this.setState({loading:true})
        PostStore.updateBrandId(id,name);
        await PostStore.load();
        this.setState({loading:false})
        this.props.navigation.navigate('Post')
    }

    render() {
        if(this.state.loading){
            return(<Loading visible={this.state.loading}></Loading>)
        }
        return (
            <View style={{ flex: 1 }}>
                <ScrollView style={{ padding: 10, backgroundColor: 'white' }}>
                    <HSm textAlign='left'>Popular Brands</HSm>
                    <View style={{ padding: 20 }}>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between' }}>
                            {
                                this.state.brands.map((brand, index) =>
                                    <View style={{ width: '30%', height: 70 }} key={index}>
                                        <TouchableOpacity onPress={()=>this.searchWithBrand(brand.id, brand.name)}>
                                            <BrandCard img={{ uri: BRAND_IMG_URL + brand.name.toLowerCase() + '.jpg' }}></BrandCard>
                                        </TouchableOpacity>
                                    </View>
                                )
                            }
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

export default Home;
