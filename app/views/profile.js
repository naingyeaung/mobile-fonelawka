import React, { Component } from 'react';
import { View, Text, Image, ScrollView } from 'react-native';
import { API_IMG_URL, PRIMARY_COLOR } from '../components/common';
import UserStore from '../mobx/UserStore';
import { observer } from 'mobx-react';
import { PLg } from '../components/text';
import { TextField } from 'react-native-material-textfield';
import { Button, BorderButton } from '../components/button';
import Br from '../components/br';
import Loading from '../components/loading';
import { NavigationEvents } from 'react-navigation';
import { authenticate } from '../services/atthenticate';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading:false
    };
  }

  componentDidMount() {
    console.log(API_IMG_URL + UserStore.User.avatar)
  }

  async auth(){
    var result = await authenticate();
    if(result==true){

    }else{
        this.props.navigation.navigate("Login");
    }
}

  async editProfile() {
    this.setState({loading:true});
    await UserStore.editUser();
    await UserStore.load();
    this.setState({loading:false})
  }

  render() {
    var user = UserStore.User;
    if (this.state.loading) {
      return (<Loading visible={this.state.loading} />)
    }
    return (
      <View style={{ flex: 1, paddingTop: 20 }}>
        <NavigationEvents
                    onWillFocus={()=>this.auth()}
                />
        <ScrollView keyboardShouldPersistTaps="handled">
          <View style={{ height: 100, alignItems: 'center' }}>
            <Image style={{ width: 100, height: 100 }} source={{ uri: user.avatar }}></Image>
          </View>
          <View style={{ padding: 20 }}>
            <TextField label='Name' value={user.name} onChangeText={(value) => UserStore.updateName(value)}></TextField>
            {/* <TextField label='Description' value={user.description != null ? user.description : ''}></TextField> */}
            <TextField label='Address' value={user.homeandStreet != null ? user.homeandStreet : ''} onChangeText={(value) => UserStore.updateAddress(value)}></TextField>
            <Br height={10}></Br>
            {
              UserStore.Phones.map((phone, index) => <View>
                <TextField label={'Phone Number ' + parseInt(index+1)} value={phone.phoneNumber!=null?phone.phoneNumber:''} onChangeText={(value) => UserStore.updatePhone(index, value)}></TextField>
              </View>)
            }
            <Br height={10}></Br>
            <Button text='Edit Profile' onPress={() => this.editProfile()}></Button>
            <Br height={20}></Br>
            <BorderButton onPress={() => this.props.navigation.navigate('Logout')} text='Logout'></BorderButton>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default observer(Profile);
