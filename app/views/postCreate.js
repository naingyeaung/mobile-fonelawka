import React, { Component } from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity, NativeModules, TextInput, KeyboardAvoidingView } from 'react-native';
import { Button } from '../components/button';
import { TextField } from 'react-native-material-textfield';
import { Dropdown } from 'react-native-material-dropdown';
import { getData, postFormData, postData } from '../services/fetch';
import { API_URL, PRIMARY_COLOR, DEFAULT_TEXT_COLOR } from '../components/common';
import Select2 from 'react-native-select-two';
import Br from '../components/br';
import { PReg } from '../components/text';
import Loading from '../components/loading';
import { getToken } from '../services/storage';
import ImagePicker from 'react-native-image-picker';
import { NavigationEvents } from 'react-navigation';
import { authenticate } from '../services/atthenticate';
// import DatePicker from 'react-native-date-picker';

class PostCreate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            description: '',
            price: 0,
            selectedImages: [],
            brands: [],
            phones: [],
            storages: [],
            photoNames: [],
            condition: [],
            purpose: [],
            brand: [],
            phone: [],
            storage: [],
            warranty: '',
            purpose_data: [{ id: 0, name: 'Buy' }, { id: 1, name: 'Sell' }, { id: 2, name: 'Exchange' }],
            condition_data: [{ id: 0, name: 'Old' }, { id: 1, name: 'New' }],
            loading: false,
            years: '',
            months: '',
            days: ''
        };
    }

    async auth() {
        var result = await authenticate();
        console.log(result);
        if (result == true) {

        } else {
            this.props.navigation.navigate("Login");
        }
    }


    async selectImage() {
        const options = {
            title: 'Select Avatar',
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        // ImagePicker.openPicker({
        //     multiple: true
        // }).then(async images => {
        //     console.log(images);
        //     this.setState({ selectedImages: images })
        //     await this.addPhotos();
        // });
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const url = { uri: response.uri };
                // You can also display the image using data:
                var source = 'data:image/jpeg;base64,' + response.data;
                console.log(source);
                this.state.photoNames.push({ photoName: source });
                this.setState({
                    photoNames: this.state.photoNames,
                });
            }
        });

    }

    // async addPhotos() {
    //     var RNFS = require('react-native-fs')
    //     for (var i = 0; i < this.state.selectedImages.length; i++) {
    //         console.log(this.state.selectedImages[i]);
    //         base64data = await RNFS.readFile(this.state.selectedImages[i].path, 'base64').then();
    //         this.state.photoNames.push({ photoName: 'data:image/jpeg;base64,' + base64data })
    //         this.setState({ photoNames: this.state.photoNames })
    //     }
    // }

    async creatPost() {
        // console.log(this.state.photoNames)
        var warranty = null;
        this.state.years != '' ? warranty = this.state.years + (this.state.years > 1 ? ' years ' : ' year ') : warranty;
        this.state.months != '' ? warranty += this.state.months + (this.state.months > 1 ? ' months ' : ' month ') : warranty;
        this.state.days != '' ? warranty += this.state.days + (this.state.days > 1 ? ' days' : ' day ') : warranty;

        this.setState({ loading: true })
        if (this.state.purpose.length > 0 && this.state.condition.length > 0 && this.state.brand.length > 0 && this.state.phone.length > 0 && this.state.storage.length > 0) {
            var model = {
                Title: this.state.title,
                Description: this.state.description,
                StatusType: this.state.purpose[0],
                ConditionType: this.state.condition[0],
                PhoneId: this.state.phone[0],
                Price: this.state.price,
                StorageId: this.state.storage[0],
                Photos: this.state.photoNames,
                Warranty: warranty
            }
            console.log(model);
            var response = await postData(API_URL + 'posts/createPostForMobile', model);
            this.setState({ loading: false })
            if (response.ok) {
                this.props.navigation.navigate('PostDetail', { postId: response.data });
            } else {

            }
        } else {
            alert('Please fill all valid data')
        }

    }

    async componentDidMount() {
        await this.getBrands();
        await this.getStorages();

        var token = await getToken();
        console.log(token);
    }

    async getBrands() {
        var response = await getData(API_URL + 'basic/getBrands');
        if (response.ok) {
            this.setState({ brands: response.data })
            console.log(this.state.brands)
        } else {
            console.log(response.data);
        }
    }

    async getStorages() {
        var response = await getData(API_URL + 'basic/getStorages');
        if (response.ok) {
            console.log(response.data)
            response.data.forEach(element => {
                var storage = { id: element.id, name: element.size };
                this.state.storages.push(storage);
                this.setState({ storages: this.state.storages })
            });
        } else {
            console.log(response.data);
        }
    }

    async getPhones(id) {
        console.log(id);
        this.setState({ loading: true });
        var response = await getData(API_URL + 'basic/getphoneswithbrand?brandname=' + id);
        if (response.ok) {
            this.setState({ loading: false })
            this.setState({ phones: response.data })
            console.log(this.state.phones);
        } else {
            console.log(response.data);
        }
    }


    render() {
        if (this.state.loading) {
            return (<Loading visible={this.state.loading} />)
        }
        return (
            <View style={{ justifyContent: 'flex-end', flex: 1 }}>
                <NavigationEvents
                    onWillFocus={() => this.auth()}
                />
                <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === "ios" ? "padding" : null}>
                    <ScrollView keyboardShouldPersistTaps='handled' contentContainerStyle={{ justifyContent: 'flex-end' }}>

                        <View style={{ padding: 20 }}>

                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', flexWrap: 'wrap' }}>
                                {
                                    this.state.photoNames.map((image, index) =>
                                        <View style={{ height: 110, width: 105, justifyContent: 'center', alignItems: 'center' }} key={index}>
                                            <Image source={{ uri: image.photoName }} style={{ width: 95, height: 100 }} key={index} />
                                        </View>

                                    )
                                }
                                <TouchableOpacity onPress={() => this.selectImage()}>
                                    <View style={{ height: 110, width: 105, justifyContent: 'center', alignItems: 'center' }}>
                                        <View style={{ width: 95, height: 100, borderRadius: 5, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', borderColor: DEFAULT_TEXT_COLOR, borderWidth: 0.5 }}>
                                            <Image style={{ width: 25, height: 25 }} source={require('../assets/images/camera.png')}></Image>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <TextField key label='Title' value={this.state.title} onChangeText={(value) => this.setState({ title: value })}></TextField>
                            <TextField label='Description' value={this.state.description} onChangeText={(value) => this.setState({ description: value })}></TextField>
                            <Br height={15}></Br>
                            <Select2
                                isSelectSingle
                                style={{ borderRadius: 5 }}
                                colorTheme={PRIMARY_COLOR}
                                popupTitle='Select Purpose'
                                title='Select Purpose'
                                data={this.state.purpose_data}
                                selectButtonText='Select'
                                cancelButtonText='Cancel'
                                title='Select Purpose'
                                searchPlaceHolderText='Please Select Purpose'
                                onSelect={data => {
                                    this.setState({ purpose: data });
                                }}
                                onRemoveItem={data => {
                                    this.setState({ purpose: data });
                                }}
                            />
                            <Br height={20}></Br>
                            <Select2
                                isSelectSingle
                                style={{ borderRadius: 5 }}
                                colorTheme={PRIMARY_COLOR}
                                popupTitle='Select Condition'
                                title='Select Condition'
                                data={this.state.condition_data}
                                selectButtonText='Select'
                                cancelButtonText='Cancel'
                                title='Select Condition'
                                searchPlaceHolderText='Please Select Condition'
                                onSelect={condition => {
                                    this.setState({ condition: condition });
                                }}
                                onRemoveItem={data => {
                                    this.setState({ condition: data });
                                }}
                            />
                            <Br height={20}></Br>
                            {
                                this.state.condition.length > 0 ?
                                    <View>
                                        {
                                            this.state.condition[0] == 0 ?
                                                <View>
                                                    {/* <DatePicker
                                                    date={this.state.date}
                                                    onDateChange={date => this.setState({ date })}
                                                /> */}
                                                </View>
                                                : <></>
                                        }
                                    </View> : <></>
                            }
                            <View></View>
                            <Select2
                                isSelectSingle
                                style={{ borderRadius: 5 }}
                                colorTheme={PRIMARY_COLOR}
                                popupTitle='Select item'
                                title='Select item'
                                data={this.state.brands}
                                selectButtonText='Select'
                                cancelButtonText='Cancel'
                                title='Select Brand'
                                searchPlaceHolderText='Please Select Brand'
                                onSelect={data => {
                                    this.setState({ brand: data });
                                    this.getPhones(data[0])
                                }}
                                onRemoveItem={data => {
                                    this.setState({ brand: data });
                                }}
                            />
                            <Br height={20}></Br>
                            <Select2
                                isSelectSingle
                                style={{ borderRadius: 5 }}
                                colorTheme={PRIMARY_COLOR}
                                popupTitle='Select Model'
                                title='Select Model'
                                data={this.state.phones}
                                selectButtonText='Select'
                                cancelButtonText='Cancel'
                                title='Select Model'
                                searchPlaceHolderText='Please Select Model'
                                listEmptyTitle='Please select Brand First'
                                onSelect={phone => {
                                    this.setState({ phone: phone });
                                }}
                                onRemoveItem={data => {
                                    this.setState({ phone: data });
                                }}
                            />
                            <Br height={20}></Br>
                            <Select2
                                isSelectSingle
                                style={{ borderRadius: 5 }}
                                colorTheme={PRIMARY_COLOR}
                                popupTitle='Select Storages'
                                title='Select Storages'
                                data={this.state.storages}
                                selectButtonText='Select'
                                cancelButtonText='Cancel'
                                title='Select Storages'
                                searchPlaceHolderText='Please Select Storage'
                                onSelect={storage => {
                                    this.setState({ storage: storage });
                                }}
                                onRemoveItem={data => {
                                    this.setState({ storage: data });
                                }}
                            />
                            <TextField label='Price' keyboardType='number-pad' onChangeText={(value) => this.setState({ price: value })} value={this.state.price}></TextField>
                            <Br height={25}></Br>
                            <PReg textAlign='left'>Warranty</PReg>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <TextInput value={this.state.years} onChangeText={(value) => this.setState({ years: value })} style={{ borderBottomColor: '#333', borderBottomWidth: 0.5, height: 40 }}></TextInput>
                                </View>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <PReg>Years</PReg>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <TextInput value={this.state.months} onChangeText={(value) => value < 12 ? this.setState({ months: value }) : this.setState({ months: this.state.months })} style={{ borderBottomColor: '#333', borderBottomWidth: 0.5, height: 40 }}></TextInput>
                                </View>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <PReg>Months</PReg>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <TextInput value={this.state.days} onChangeText={(value) => value < 30 ? this.setState({ days: value }) : this.setState({ days: this.state.days })} style={{ borderBottomColor: '#333', borderBottomWidth: 0.5, height: 40 }}></TextInput>
                                </View>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <PReg>Days</PReg>
                                </View>
                            </View>
                            {/* <View style={{ flexDirection: 'row' }}>
                            <TextField label='Warranty' value={this.state.warranty} onChangeText={(value) => this.setState({ warranty: value })}></TextField>

                        </View> */}
                            {/* <Dropdown
                            label='Model'
                            data={data}
                        />
                        <Dropdown
                            label='Storage'
                            data={data}
                        /> */}
                            <Br height={20}></Br>
                            <Button text='Create' onPress={() => this.creatPost()}></Button>
                        </View>
                        <View style={{height:50}}></View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        );
    }
}

export default PostCreate;
