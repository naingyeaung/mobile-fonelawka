import React, { Component } from 'react';
import { View, Text, ScrollView, RefreshControl } from 'react-native';
import { getData } from '../services/fetch';
import { API_URL, API_IMG_URL } from '../components/common';
import { NotiCard } from '../components/card';
import { NavigationEvents } from 'react-navigation';
import { authenticate } from '../services/atthenticate';
import moment from "moment";

class Notification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notifications: [],
            refreshing:false
        };
    }

    async componentDidMount() {
        await this.getNotis()
    }

    async auth(){
        var result = await authenticate();
        if(result==true){

        }else{
            this.props.navigation.navigate("Login");
        }
    }

    async getNotis() {
        this.setState({refreshing:true})
        var response = await getData(API_URL + 'notification/fetchNotis');
        if (response.ok) {
            console.log(response.data);
            this.setState({ notifications: response.data });
        }else{
            console.log(response.data);
        }
        this.setState({refreshing:false});
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <NavigationEvents
                    onWillFocus={()=>this.auth()}
                />
                <ScrollView refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={() => this.getNotis()}
                    />
                }>
                    {
                        this.state.notifications.map((noti, index) =>
                            <NotiCard message={noti.message} isSeen={noti.isSeen} date={moment(noti.createdOn, "YYYYMMDD,h:mm:ss").fromNow()} img={{ uri: noti.photolink.startsWith('/')? API_IMG_URL+noti.photolink:noti.photolink }} key={index}></NotiCard>
                        )
                    }
                </ScrollView>

            </View>
        );
    }
}

export default Notification;
