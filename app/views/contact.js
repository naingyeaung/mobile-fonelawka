import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { ContactCard } from '../components/card';
import { BACKGROUND_COLOR } from '../components/common';

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{flex:1,padding:20, backgroundColor:BACKGROUND_COLOR}}>
        <ContactCard img={require('../assets/images/chat.png')} text={'Send Message to Seller'}></ContactCard>
      </View>
    );
  }
}

export default Contact;
