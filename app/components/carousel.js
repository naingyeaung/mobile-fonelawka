import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { API_URL } from './common';
import { PReg } from './text';

export class PostCarousel extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        console.log(this.props.data)
    }

    _renderItem({item, index}) {
        return(
            <Image source={{uri:API_URL + 'img/' + item.path + '/' + item.photoName}} style={{width:'100%',height:'100%'}} resizeMode='cover'></Image>
        )
        
    }

    render() {
        return (
            <View>
                <Carousel
                    ref={(c) => { this._carousel = c; }}
                    data={this.props.data}
                    layout='default'
                    renderItem={this._renderItem}
                    sliderWidth={this.props.sliderWidth}
                    itemWidth={this.props.itemWidth}
                    sliderHeight={this.props.sliderHeight}
                    itemHeight={this.props.itemHeight}
                />
            </View>
        );
    }
}

