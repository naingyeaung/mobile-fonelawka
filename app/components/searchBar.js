import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput } from 'react-native';
import { PRIMARY_COLOR, SECONDARY_COLOR } from './common';
import PostStore from '../mobx/PostStore';
import { observer } from 'mobx-react';

class SearchBar extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    async clearSearch() {
        PostStore.updateContext('');
        await PostStore.load()
    }

    render() {
        return (
            <View style={{ flexDirection: 'row', backgroundColor: 'white', borderRadius: 15, elevation: 10, height: 35, width: '100%' }}>
                <TouchableOpacity style={{ width: '10%' }} onPress={() => PostStore.load()}>
                    <View style={{ backgroundColor: 'white', height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 15 }}>
                        <Image source={require('../assets/images/search.png')} style={{ height: 20, width: 20 }}></Image>
                    </View>
                </TouchableOpacity>
                <TextInput value={PostStore.Context} onChangeText={(value) => PostStore.updateContext(value)} id='search-text' style={{ backgroundColor: 'white', width: '80%', height: 35, borderRadius: 15 }}></TextInput>
                {
                    PostStore.Context != '' ? <TouchableOpacity style={{ width: '10%' }} onPress={() => this.clearSearch()}>
                        <View style={{ backgroundColor: 'white', height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 15 }}>
                            <Image source={require('../assets/images/cross.png')} style={{ height: 20, width: 20 }}></Image>
                        </View>
                    </TouchableOpacity> : <></>
                }

            </View>
        );
    }
}

export default observer(SearchBar);
