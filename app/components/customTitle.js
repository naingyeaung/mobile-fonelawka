import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { HSm } from './text';
import { PRIMARY_COLOR } from './common';

class CustomTitle extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <View style={{ width: 5, height: 20, backgroundColor: PRIMARY_COLOR }}></View>
                <View style={{ paddingLeft: 8, justifyContent: 'center' }}>
                    <HSm fontWeight='bold'>{this.props.text}</HSm>
                </View>
            </View>
        );
    }
}

export default CustomTitle;
