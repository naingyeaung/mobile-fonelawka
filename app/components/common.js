export const BACKGROUND_COLOR = '#f5f5f5'
export const PRIMARY_COLOR = '#0088C1'
export const SECONDARY_COLOR ='white'
export const DEFAULT_TEXT_COLOR = '#494949'
export const PRIMARY_TEXT_COLOR = 'white'
export const DEFAULT_FONT = 'Montserrat-Light'
export const DEFAULT_BADGE_COLOR = '#333'
export const DEFAULT_ICON_SIZE = 20

export const HLG_SIZE = 23;
export const HREG_SIZE = 20;
export const HSM_SIZE = 18;
export const PLG_SIZE = 15;
export const PREG_SIZE = 13;
export const PSM_SIZE = 11;

export const API_URL = 'https://fonelawkaapi.trifinitymyanmar.com/'

export const API_IMG_URL = 'https://fonelawka.com'

export const BRAND_IMG_URL = 'https://fdn2.gsmarena.com/vv/bigpic/'

export const CONDITION_TYPE = ['Old','New'];

export const PURPOSE_TYPE = ['Buy', 'Sell', 'Exchange']