import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { DEFAULT_BADGE_COLOR } from './common';

class Badge extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{backgroundColor:this.props.color?this.props.color:DEFAULT_BADGE_COLOR, padding:2,paddingTop:0,paddingBottom:0, borderRadius:this.props.borderRadius?this.props.borderRadius:7,width:38,height:18}}>
        {this.props.children}
      </View>
    );
  }
}

export default Badge;
