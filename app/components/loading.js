import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay';
import { PRIMARY_COLOR } from './common'

export default class Loading extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Spinner
                    visible={this.props.visible}
                    textContent={this.props.textContent ? this.props.textContent : "Loading..."}
                    textStyle={styles.spinnerTextStyle}
                    color={PRIMARY_COLOR}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    spinnerTextStyle: {
        color: PRIMARY_COLOR
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    },
});