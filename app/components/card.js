import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ImageBackground } from 'react-native';
import { BACKGROUND_COLOR, DEFAULT_TEXT_COLOR, DEFAULT_ICON_SIZE, DEFAULT_BADGE_COLOR, PRIMARY_COLOR, API_IMG_URL, API_URL } from './common';
import { HReg, HSm, PReg, PLg, PSm } from './text';
import Badge from './badge';
import Br from './br';
import { getData } from '../services/fetch';

export class BrandCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{ backgroundColor: BACKGROUND_COLOR, height: 50, justifyContent: 'center', width: '100%', alignContent: 'center' }}>
        <Image style={{ width: '70%', height: 50, alignSelf: 'center' }} source={this.props.img} resizeMode='cover'></Image>
      </View>
    );
  }
}

export class PostCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: this.props.count,
      totalcount: this.props.totalcount,
      disabled: false
    }
  }

  async like() {
    this.setState({ disabled: true })
    this.state.count == 0 ? this.setState({ count: 1 }) : this.setState({ count: 0 });
    var response = await getData(API_URL + 'posts/like/' + this.props.id);
    if (response.ok) {
      this.setState({ disabled: false })
    } else {
      this.setState({ disabled: false })
    }
  }

  render() {
    return (
      <View style={{ backgroundColor: 'white', height: 200 }}>
        <View style={{ flex: 4, flexDirection: 'row', borderBottomColor: '#DCDCDC', borderBottomWidth: 0.5 }}>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <ImageBackground style={{ width: '100%', height: '100%', alignSelf: 'center' }} source={this.props.img} resizeMode='cover'>
              <View style={{ flex: 5, justifyContent: 'flex-start' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                  <TouchableOpacity onPress={() => this.like()} disabled={this.state.disabled}>
                    <View style={{ width: 25, height: 25, justifyContent: 'center', alignItems: 'center', opacity: 1, backgroundColor: this.state.count > 0 ? 'transparent' : '#999' }}>
                      <Image style={{ width: 20, height: 20 }} source={this.state.count > 0 ? require('../assets/images/fav_blue.png') : require('../assets/images/fav_white.png')}></Image>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{ flex: 1, opacity: 0.5, backgroundColor: 'black', justifyContent: 'center' }}>
                <PReg color='white'>{this.props.phone}</PReg>
              </View>
            </ImageBackground>
          </View>
          {/* <View style={{ flex: 2, padding: 10 }}>

            <View style={{ flexDirection: 'row' }}>
              <PLg textAlign='left'>BlackBerry</PLg>
              <View style={{ width: 3 }}></View>
              <Badge color={this.props.condition == 'Old' ? DEFAULT_BADGE_COLOR : PRIMARY_COLOR}>
                <PSm color='white'>{this.props.condition}</PSm>
              </Badge>
            </View>
            <Br height={5}></Br>
            <PReg textAlign='left'>{this.props.phone}</PReg>

            <Br height={5}></Br>
            <PReg textAlign='left'>{this.props.seller}</PReg>
          </View> */}
        </View>
        {/* <View style={{flex:1,borderBottomColor: '#DCDCDC', borderBottomWidth: 0.5, justifyContent:'center', alignItems:'center'}}>
          <PReg textAlign='left'>{this.props.phone}</PReg>
        </View> */}
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
          <View style={{ flex: 2, justifyContent: 'center', borderRightColor: '#DCDCDC', borderRightWidth: 0.5 }}>
            <PLg>{this.props.price} ks</PLg>
          </View>
          {/* <TouchableOpacity style={{ flex: 1 }}>
            <View style={{ flex: 1, justifyContent: 'center', borderRightColor: '#DCDCDC', borderRightWidth: 0.5, flexDirection: 'row', alignItems: 'center' }}>
              <Image style={{ width: DEFAULT_ICON_SIZE, height: DEFAULT_ICON_SIZE }} source={require('../assets/images/fav_blue.png')}></Image>
              <PSm>{this.props.totalcount}</PSm>
            </View>
          </TouchableOpacity> */}
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Contact')} style={{ flex: 1 }}>
            <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', alignItems: 'center' }}>
              <Image style={{ width: DEFAULT_ICON_SIZE, height: DEFAULT_ICON_SIZE }} source={require('../assets/images/phone.png')}></Image>
              {/* <PLg> Contact</PLg> */}
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export class NotiCard extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  render() {
    return (
      <View style={{ backgroundColor: this.props.isSeen == true ? 'white' : BACKGROUND_COLOR, height: 100, flexDirection: 'row' }}>
        <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
          <Image style={{ width: 60, height: 60, borderRadius: 30 }} source={this.props.img}></Image>
        </View>
        <View style={{ flex: 5, justifyContent: 'center' }}>
          <PReg textAlign='left'>{this.props.message}</PReg>
          <Br height={10}></Br>
          <PReg textAlign='left'>{this.props.date}</PReg>
        </View>
      </View>
    )
  }
}

export class ContactCard extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  render() {
    return (
      <View style={{ flexDirection: 'row', height: 50, backgroundColor: 'white' }}>
        <View style={{ flex: 1, justifyContent: 'center', padding: 10, alignItems: 'flex-start', paddingLeft: 20 }}>
          <Image style={{ width: DEFAULT_ICON_SIZE, height: DEFAULT_ICON_SIZE }} source={this.props.img}></Image>
        </View>
        <View style={{ flex: 4, justifyContent: 'center' }}>
          <PReg textAlign='left'>{this.props.text}</PReg>
        </View>
      </View>
    )
  }
}

export class FilterCard extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  render() {
    return (
      <View style={{ elevation: 5, backgroundColor: 'white', borderRadius: 5, padding: 5 }}>
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
          <PReg>{this.props.text}</PReg>
          <View style={{ width: 10 }}></View>
          <TouchableOpacity onPress={this.props.onPress}>
            <Image style={{ width: 15, height: 15 }} source={require('../assets/images/cross.png')}></Image>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export class CommentCard extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  render() {
    return (
      <View>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 1, alignItems: this.props.isReply ? 'center' : 'flex-start' }}>
            <Image source={this.props.img} style={{ width: this.props.isReply ? 20 : 30, height: this.props.isReply ? 20 : 30, borderRadius: 50 }}></Image>
          </View>
          <View style={{ flex: 6 }}>
            <PLg textAlign='left' fontWeight='bold'>{this.props.name}</PLg>
            <Br height={10}></Br>
            <PReg textAlign='left'>{this.props.comment}</PReg>
            <Br height={10}></Br>
            <TouchableOpacity onPress={this.props.onReplyPress}>
              <PReg textAlign='left' fontWeight='bold'>Reply</PReg>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}
