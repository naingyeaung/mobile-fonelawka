import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { PRIMARY_COLOR, PREG_SIZE, DEFAULT_TEXT_COLOR, PRIMARY_TEXT_COLOR, DEFAULT_FONT, DEFAULT_ICON_SIZE } from './common';

export class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} disabled={this.props.disabled ? this.props.disabled : false}>
                <View style={{ backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : PRIMARY_COLOR, height: this.props.height ? this.props.height : 40, justifyContent: 'center', alignItems: 'center', opacity: this.props.disabled == true ? 0.7 : 1, flexDirection: 'row' }}>
                    {
                        this.props.img != null && this.props.img != '' ?
                            <View style={{flexDirection:'row'}}>
                                <Image style={{ width: DEFAULT_ICON_SIZE, height: DEFAULT_ICON_SIZE }} source={this.props.img}></Image>
                                <View style={{ width: 10 }}></View>
                            </View>

                            : <></>
                    }
                    <Text style={{ fontSize: this.props.fontSize ? this.props.fontSize : PREG_SIZE, color: this.props.color ? this.props.color : PRIMARY_TEXT_COLOR, textAlign: 'center', fontFamily: this.props.fontFamily ? this.props.fontFamily : DEFAULT_FONT }}>{this.props.text}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

export class BorderButton extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} disabled={this.props.disabled ? this.props.disabled : false}>
                <View style={{ borderColor: this.props.backgroundColor ? this.props.backgroundColor : PRIMARY_COLOR, height: this.props.height ? this.props.height : 40, justifyContent: 'center', alignItems: 'center', opacity: this.props.disabled == true ? 0.7 : 1, flexDirection: 'row', borderWidth: 1 }}>
                    {
                        this.props.img != null && this.props.img != '' ?
                            <View style={{flexDirection:'row'}}>
                                <Image style={{ width: DEFAULT_ICON_SIZE, height: DEFAULT_ICON_SIZE }} source={this.props.img}></Image>
                                <View style={{ width: 10 }}></View>
                            </View>
                            : <></>
                    }
                    <Text style={{ fontSize: this.props.fontSize ? this.props.fontSize : PREG_SIZE, color: this.props.color ? this.props.color : PRIMARY_COLOR, textAlign: 'center', fontFamily: this.props.fontFamily ? this.props.fontFamily : DEFAULT_FONT }}>{this.props.text}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}