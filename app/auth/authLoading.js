import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { getToken } from '../services/storage';
import { getData } from '../services/fetch';
import { API_URL } from '../components/common';
import UserStore from '../mobx/UserStore';
import AsyncStorage from '@react-native-community/async-storage';

class AuthLoading extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  async componentDidMount() {
    var token = await getToken();
    if (token != null) {
      await UserStore.load();
      var agreed = await AsyncStorage.getItem('agree');
      this.props.navigation.navigate(agreed=='true'?'User':'Terms');
    } else {
      this.props.navigation.navigate('User')
    }
  }

  render() {
    return (
      <View>

      </View>
    );
  }
}

export default AuthLoading;
