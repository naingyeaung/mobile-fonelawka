import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import { Button } from '../components/button';
import { postData, getData } from '../services/fetch';
import { API_URL, PRIMARY_COLOR } from '../components/common';
import AsyncStorage from '@react-native-community/async-storage';
import { setToken } from '../services/storage';
import Br from '../components/br';
import { LoginManager, AccessToken } from 'react-native-fbsdk'
import Loading from '../components/loading';
import { PReg } from '../components/text';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      password: '',
      loading: false
    };

    this.loginFacebook = this.loginFacebook.bind(this)
  }

  loginFacebook = async () => {
    try {
      let result = await LoginManager.logInWithPermissions(["public_profile", "email"])
      if (result.isCancelled) {
        alert('login was cancelled');
      } else {
        AccessToken.getCurrentAccessToken().then(async (data) => {
          console.log(data.accessToken);
          var fbbuser = await this.initUser(data.accessToken);
          var loginprovider = 'Facebook';
          var providerkey = data.accessToken;
          var email = fbbuser.email;
          var role = 'User';
          var model = {
            loginprovider,
            providerkey,
            email,
            role,
            fbbuser
          }
          console.log(model)
          this.setState({ loading: true })
          var response = await postData(API_URL + 'account/facebookLogin', model);
          if (response.ok) {
            console.log(response.data.token);
            await setToken(response.data.token);
            await AsyncStorage.setItem('role', response.data.role);
            var agreed = await AsyncStorage.getItem('agree');
            this.setState({ loading: false })
            this.props.navigation.navigate(this.agreed=='true'?'AuthLoading':'Terms');
          } else {
            console.log(response.data)
          }
        });
        // this.props.navigation.navigate('CheckOut');
      }
    }
    catch (error) {
      alert('Login failed width error:' + error)
    }
  }

  async initUser(token) {
    var name = '';
    var email = '';
    var id = '';
    var address = '';
    var phone = '';
    var township = '';
    await fetch('https://graph.facebook.com/v3.3/me?fields=email,name&access_token=' + token)
      .then((response) => response.json())
      .then((json) => {
        name = json.name;
        email = json.email;
        id = json.id;
      })
      .catch((error) => {
      })
    await fetch('https://graph.facebook.com/v3.3/' + id + '/picture?redirect=false')
      .then((response) => response.json())
      .then((json) => {
        avatar = json.data.url
      })
    var user = { name, email, avatar };
    console.log(user);
    return user;
  }

  async login() {
    const model = {
      Phno: this.state.phone,
      Password: this.state.password
    }

    var response = await postData(API_URL + 'account/login', model);
    if (response.ok) {
      console.log(response.data);
      await setToken(response.data);
      this.props.navigation.navigate('AuthLoading')
    } else {
      console.log(response.data)
    }
  }

  render() {
    if (this.state.loading) {
      return (<Loading visible={this.state.loading} />)
    }
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Image style={{ width: 90, height: 90, alignSelf: 'center' }} source={require('../assets/images/logo-03.png')}></Image>
        <Br height={10}></Br>
        <PReg color={PRIMARY_COLOR}>F O N E L A W K A . C O M</PReg>
        {/* <TextField label="Phone Number" onChangeText={(value) => this.setState({ phone: value })} value={this.state.phone}></TextField>
        <TextField label="Password" onChangeText={(value) => this.setState({ password: value })} value={this.state.password}></TextField> */}
        <View style={{ padding: 30 }}>
          <Button text='Login With Facebook' onPress={() => this.loginFacebook()}></Button>
          <Br height={20}></Br>
          <Button text='Continue as guest' onPress={() => this.props.navigation.navigate('AuthLoading')}></Button>
        </View>

      </View>
    );
  }
}

export default Login;
