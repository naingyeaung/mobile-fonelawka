import React, { Component } from 'react';
import { View, Text, Image, CheckBox } from 'react-native';
import UserStore from '../mobx/UserStore';
import { TextField } from 'react-native-material-textfield';
import { Button } from '../components/button';
import { PReg } from '../components/text';
import Br from '../components/br';
import AsyncStorage from '@react-native-community/async-storage';
import RoundCheckbox from 'rn-round-checkbox';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

class Terms extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: true
        };
    }

    async continue() {
        await AsyncStorage.setItem('agree', 'true');
        this.props.navigation.navigate('AuthLoading');
    }

    render() {
        var user = UserStore.User;
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 100, alignItems: 'center', padding: 20 }}>
                    <Image style={{ width: 100, height: 100 }} source={{ uri: user.avatar }}></Image>
                </View>
                <View style={{ padding: 20 }}>
                    <PReg textAlign='left' fontWeight='bold'>Name</PReg>
                    <PReg textAlign='left'>{user.name}</PReg>
                    <Br height={10}></Br>
                    <PReg textAlign='left' fontWeight='bold'>Description</PReg>
                    <PReg textAlign='left'>{user.description == '' ? '-' : user.description}</PReg>
                    <Br height={10}></Br>
                    <PReg textAlign='left' fontWeight='bold'>Address</PReg>
                    <PReg textAlign='left'>{user.homeandStreet == '' ? '-' : user.homeandStreet}</PReg>
                    <Br height={10}></Br>
                    <PReg textAlign='left' fontWeight='bold'>Temrs and Conditions</PReg>
                    <PReg textAlign='left'>
                        For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to Profile Picture and email address of facebook and phone number of a user. The information that we request will be retained by us and used as described in this privacy policy. The app does use third party services that may collect information used to identify you. Link to privacy policy of third party service providers used by the app Google Play Services Firebase Analytics Facebook
                    </PReg>
                    <PReg textAlign='left'>
                        https://fonelawka.com/home/privacy
                    </PReg>
                    <Br height={10}></Br>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableWithoutFeedback onPress={() => this.setState({ checked: !this.state.checked })}>
                            <RoundCheckbox
                                size={24}
                                checked={this.state.checked}
                            />
                        </TouchableWithoutFeedback>
                        <View style={{ width: 10 }}></View>
                        <PReg fontWeight='bold'>Agree to our terms and conditions</PReg>
                    </View>
                    <Br height={10}></Br>
                    {/* <TextField label='Description' value={user.description != null ? user.description : ''}></TextField>
                    <TextField label='Address' value={user.homeandStreet != null ? user.homeandStreet : ''}></TextField> */}
                    <Button disabled={this.state.checked == true ? false : true} onPress={() => this.continue()} text='Continue'></Button>
                </View>

            </View >
        );
    }
}

export default Terms;
