import React, { Component } from 'react';
import { View } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';


class Logout extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.logout();
    }

    async logout() {
        try {
            await AsyncStorage.clear();
            const { navigation } = this.props;
            navigation.navigate('AuthLoading');
        } catch (error) {
            // Error saving data
        }
    }

    render() {
        return (
            <View>
            </View>
        )
    }
}

export default Logout