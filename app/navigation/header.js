import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, TextInput } from 'react-native';
import { PRIMARY_COLOR, SECONDARY_COLOR, BACKGROUND_COLOR, DEFAULT_ICON_SIZE, API_URL } from '../components/common';
import ModalWrapper from 'react-native-modal-wrapper';
import { HReg, PReg, HLg, HSm, PLg } from '../components/text';
import SearchBar from '../components/searchBar';
import Br from '../components/br';
import { styles } from '../components/styles';
import { Button } from '../components/button';
import { getData } from '../services/fetch';
import Select2 from 'react-native-select-two';
import PostStore from '../mobx/PostStore';
import Loading from '../components/loading';

export class HomeHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{ heihg: 250, backgroundColor: PRIMARY_COLOR, paddingTop: 30, justifyContent: 'center', padding: 20 }}>
        <Image style={{ width: 60, height: 60, alignSelf: 'center' }} source={require('../assets/images/logo-04.png')}></Image>
        <HSm color={SECONDARY_COLOR}>fonelawka</HSm>
        <PReg color={SECONDARY_COLOR}>Find, Buy, Sell, Exchange</PReg>
        <Br height={15}></Br>
        <SearchBar></SearchBar>
      </View>
    );
  }
}

export class Header extends Component {
  render() {
    return (
      <View style={{ backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : PRIMARY_COLOR, height: 40, justifyContent: 'center' }}>
        <PReg color={this.props.color ? this.props.color : 'white'} textAlign='center'>{this.props.title}</PReg>
      </View>
    )
  }
}

export class HeaderBack extends Component {
  render() {
    return (
      <View style={{ flexDirection: 'row', backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : PRIMARY_COLOR, height: 40, justifyContent: 'center' }}>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <TouchableOpacity onPress={()=>this.props.navigation.goBack(null)}>
            <Image style={{ width: DEFAULT_ICON_SIZE+5, height: DEFAULT_ICON_SIZE+5 }} source={require('../assets/images/back.png')}></Image>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <PReg color={this.props.color ? this.props.color : 'white'} textAlign='center'>{this.props.title}</PReg>
        </View>
        <View style={{ flex: 1 }}></View>

      </View>
    )
  }
}

export class FilterHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      brands: [],
      phones: [],
      brand: [],
      phone: [],
      min: 0,
      max: 0,
      loading: false
    }
  }

  async componentDidMount() {
    await this.getBrands();
  }

  async getBrands() {
    var response = await getData(API_URL + 'basic/getBrands');
    if (response.ok) {
      this.setState({ brands: response.data })
      console.log(this.state.brands)
    } else {
      console.log(response.data);
    }
  }

  async getPhones(id) {
    console.log(id);
    this.setState({ loading: true });
    var response = await getData(API_URL + 'basic/getphoneswithbrand?brandname=' + id);
    if (response.ok) {
      this.setState({ loading: false })
      this.setState({ phones: response.data })
      console.log(this.state.phones);
    } else {
      console.log(response.data);
    }
  }

  async filter() {
    var brandid = this.state.brand.length > 0 ? this.state.brand[0] : '';
    var phoneid = this.state.phone.length > 0 ? this.state.phone[0] : '';
    console.log('min', this.state.min)
    console.log('max', this.state.max);
    PostStore.updateFilter(brandid, phoneid, this.state.min, this.state.max);

    await PostStore.load();

    this.setState({ modalVisible: false });
  }

  async clearFilter() {
    this.setState({ min: '' });
    this.setState({ max: '' });
    this.setState({ brand: [] });
    this.setState({ phone: [] });
    PostStore.updateFilter('', '', 0, 0);

    await PostStore.load();

    this.setState({ modalVisible: false });
  }

  render() {
    return (
      <View style={{ backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : PRIMARY_COLOR, height: 50, justifyContent: 'center', flexDirection: 'row' }}>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <Image style={{ width: 35, height: 35, alignSelf: 'center' }} source={require('../assets/images/logo-04.png')}></Image>
        </View>
        <View style={{ flex: 5, justifyContent: 'center' }}>
          <SearchBar></SearchBar>
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.setState({ modalVisible: true })}>
            <Image source={require('../assets/images/compare_white.png')} style={{ width: DEFAULT_ICON_SIZE, height: DEFAULT_ICON_SIZE }}></Image>
          </TouchableOpacity>
        </View>
        <ModalWrapper
          containerStyle={{ flexDirection: 'row', justifyContent: 'flex-end' }}
          onRequestClose={() => this.setState({ modalVisible: false })}
          position="right"
          style={styles.sidebar}
          visible={this.state.modalVisible}>
          <View style={{ flex: 1, padding: 10 }}>
            <View style={{ height: 40, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}></View>
              <View style={{ flex: 1 }}></View>
              <View style={{ flex: 1 }}></View>
            </View>
            <PLg textAlign='left'>CATEGORIES</PLg>
            <Br height={10}></Br>
            <Select2
              isSelectSingle
              style={{ borderRadius: 5 }}
              colorTheme={PRIMARY_COLOR}
              popupTitle='Select item'
              title='Select item'
              data={this.state.brands}
              selectButtonText='Select'
              cancelButtonText='Cancel'
              title='Select Brand'
              searchPlaceHolderText='Please Select Brand'
              onSelect={data => {
                this.setState({ brand: data });
                this.getPhones(data[0])
              }}
              onRemoveItem={data => {
                this.setState({ brand: data });
              }}
            />
            <Br height={20}></Br>
            <Select2
              isSelectSingle
              style={{ borderRadius: 5 }}
              colorTheme={PRIMARY_COLOR}
              popupTitle='Select Model'
              title='Select Model'
              data={this.state.phones}
              selectButtonText='Select'
              cancelButtonText='Cancel'
              title='Select Model'
              searchPlaceHolderText='Please Select Model'
              listEmptyTitle='Please select Brand First'
              onSelect={phone => {
                this.setState({ phone: phone });
              }}
              onRemoveItem={data => {
                this.setState({ phone: data });
              }}
            />
            <View style={{ borderWidth: 1, borderColor: BACKGROUND_COLOR, height: 20 }}></View>
            <PLg textAlign='left'>PRICE</PLg>
            <Br height={10}></Br>
            <TextInput onChangeText={(value) => this.setState({ min: JSON.parse(JSON.stringify(value)) })} value={this.state.min} keyboardType='number-pad' style={{ height: 40, borderColor: '#999', borderWidth: 0.7, borderRadius: 5, paddingLeft: 20 }} placeholder='Minimum'></TextInput>
            <Br height={20}></Br>
            <TextInput onChangeText={(value) => this.setState({ max: JSON.parse(JSON.stringify(value)) })} value={this.state.max} keyboardType='number-pad' style={{ height: 40, borderColor: '#999', borderWidth: 0.7, borderRadius: 5, paddingLeft: 20 }} placeholder='Maximum'></TextInput>
            <Br height={20}></Br>
            <Button text='Show' onPress={() => this.filter()}></Button>
            <Br height={20}></Br>
            <Button text='Clear Filter' onPress={() => this.clearFilter()}></Button>
          </View>
        </ModalWrapper>
      </View>
    )
  }
}