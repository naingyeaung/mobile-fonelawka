import React, { Component } from 'react'
import { Image, View } from 'react-native'
import { createStackNavigator, createAppContainer, createBottomTabNavigator, createSwitchNavigator } from 'react-navigation'
import { PSm } from '../components/text';
import { PRIMARY_COLOR } from '../components/common';
import Home from '../views/home';
import { HomeHeader, Header, FilterHeader, HeaderBack } from './header';
import Post from '../views/post';
import PostDetail from '../views/postDetail';
import Login from '../auth/login';
import PostCreate from '../views/postCreate';
import AuthLoading from '../auth/authLoading';
import Logout from '../auth/logout';
import Notification from '../views/notification';
import Profile from '../views/profile';
import Contact from '../views/contact';
import Terms from '../auth/terms';
import Shop from '../views/shop';


const AuthLoadingStack = createStackNavigator({
    AuthLoading: {
        screen: AuthLoading,
        navigationOptions: {
            header: null
        }
    }
})

const LoginStack = createStackNavigator({
    Login: {
        screen: Login,
        navigationOptions: {
            header: null
        }
    },
    Terms: {
        screen: Terms,
        navigationOptions: {
            header: null
        }
    }
})

// const LogoutStack = createStackNavigator({
//     Logout: {
//         screen: logout,
//         navigationOptions: {
//             header: null
//         }
//     }
// })

const HomeStack = createStackNavigator({
    Home: {
        screen: Home,
        navigationOptions: {
            header: (({ navigation }) => <HomeHeader navigation={navigation}></HomeHeader>)
        }
    },
});

const PostStack = createStackNavigator({
    Post: {
        screen: Post,
        navigationOptions: {
            header: (({ navigation }) => <FilterHeader title='Posts' navigation={navigation}></FilterHeader>)
        }
    },
    PostDetail: {
        screen: PostDetail,
        navigationOptions: {
            header: (({ navigation }) => <HeaderBack title="" navigation={navigation} backgroundColor='white'></HeaderBack>)
        }
    },
    Contact: {
        screen: Contact,
        navigationOptions: {
            header: (({ navigation }) => <HeaderBack title="" navigation={navigation} backgroundColor='white'></HeaderBack>)
        }
    },
    Shop:{
        screen:Shop,
        navigationOptions:{
            header : null
        }
    }
})

PostStack.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        tabBarVisible = false;
    }

    return {
        tabBarVisible,
    };
};

const PostCreateStack = createStackNavigator({
    PostCreate: {
        screen: PostCreate,
        navigationOptions: {
            header: (({ navigation }) => <Header title="" navigation={navigation} backgroundColor='white'></Header>)
        }
    }
})

const NotificationStack = createStackNavigator({
    Notification: {
        screen: Notification,
        navigationOptions: {
            header: (({ navigation }) => <Header title="Notifications" navigation={navigation}></Header>)
        }
    }
})


const ProfileStack = createStackNavigator({
    Profile: {
        screen: Profile,
        navigationOptions: {
            header: (({ navigation }) => <Header title="Profile" navigation={navigation}></Header>)
        }
    },
    Logout: {
        screen: Logout,
        navigationOptions: {
            header: null
        }
    }
})


const UserStack = createBottomTabNavigator({
    Home: {
        screen: HomeStack,
        navigationOptions: {
            tabBarIcon: ({ focused, tintColor }) => (
                <TabBarIcon focused={focused} focusedIcon={require('../assets/images/search_blue.png')} icon={require('../assets/images/search.png')}></TabBarIcon>
            ),
            tabBarLabel: ({ focused }) => (
                <View></View>
            )
        }
    },
    Post: {
        screen: PostStack,
        navigationOptions: {
            tabBarIcon: ({ focused, tintColor }) => (
                <TabBarIcon focused={focused} focusedIcon={require('../assets/images/news_blue.png')} icon={require('../assets/images/news.png')}></TabBarIcon>
            ),
            tabBarLabel: ({ focused }) => (
                <View></View>
            )
        }
    },
    PostCreate: {
        screen: PostCreateStack,
        navigationOptions: {
            tabBarIcon: ({ focused }) => (
                // <View style={{ paddingBottom: 15 }}>
                //     <View style={{ width: 60, height: 60, borderRadius: 30, justifyContent: 'center' }}>
                //         <View style={{ width: 50, height: 50, borderRadius: 25, justifyContent: 'center', alignContent: 'center', backgroundColor: PRIMARY_COLOR }}>
                //             <Image source={require('../assets/images/create.png')} style={{ width: 25, height: 25, alignSelf: 'center' }}></Image>
                //         </View>
                //     </View>
                // </View>
                <TabBarIcon focused={focused} focusedIcon={require('../assets/images/upload_blue.png')} icon={require('../assets/images/upload.png')}></TabBarIcon>
            ),
            tabBarLabel: ({ focused }) => (
                <View></View>
            )
        }
    },
    Notification: {
        screen: NotificationStack,
        navigationOptions: {
            tabBarIcon: ({ focused, tintColor }) => (
                <TabBarIcon focused={focused} focusedIcon={require('../assets/images/noti_blue.png')} icon={require('../assets/images/noti.png')}></TabBarIcon>
            ),
            tabBarLabel: ({ focused }) => (
                <View></View>
            )
        }
    },

    Profile: {
        screen: ProfileStack,
        navigationOptions: {
            tabBarIcon: ({ focused, tintColor }) => (
                <TabBarIcon focused={focused} focusedIcon={require('../assets/images/profile_blue.png')} icon={require('../assets/images/profile.png')}></TabBarIcon>
            ),
            tabBarLabel: ({ focused }) => (
                <View></View>
            )
        }
    }
},
    {
        tabBarOptions: {
            activeTintColor: PRIMARY_COLOR,
            inactiveTintColor: 'gray',
            style: { elevation: 10, borderRadius: 0, borderTopWidth: 0, height: 40, position: 'relative', justifyContent: 'center', alignItems: 'center' },

        },
    }
);

const AnonymousStack = createBottomTabNavigator({
    Home: {
        screen: HomeStack,
        navigationOptions: {
            tabBarIcon: ({ focused, tintColor }) => (
                <TabBarIcon focused={focused} focusedIcon={require('../assets/images/search_blue.png')} icon={require('../assets/images/search.png')}></TabBarIcon>
            ),
            tabBarLabel: ({ focused }) => (
                <View style={{ marginTop: -10, justifyContent: 'center', alignItems: 'center' }}>
                    <PSm color={focused ? PRIMARY_COLOR : 'black'} fontWeight='bold'>

                    </PSm>
                </View>
            )
        }
    },
    Post: {
        screen: PostStack,
        navigationOptions: {
            tabBarIcon: ({ focused, tintColor }) => (
                <TabBarIcon focused={focused} focusedIcon={require('../assets/images/news_blue.png')} icon={require('../assets/images/news.png')}></TabBarIcon>
            ),
            tabBarLabel: ({ focused }) => (
                <View style={{ marginTop: -10, justifyContent: 'center', alignItems: 'center' }}>
                    <PSm color={focused ? PRIMARY_COLOR : 'black'} fontWeight='bold'>

                    </PSm>
                </View>
            )
        }
    },
},
    {
        tabBarOptions: {
            activeTintColor: PRIMARY_COLOR,
            inactiveTintColor: 'gray',
            style: { elevation: 10, borderRadius: 0, borderTopWidth: 0, height: 40, position: 'relative', justifyContent: 'center', alignItems: 'center' },

        },
    }
)

const Root = createSwitchNavigator({
    AuthLoading: {
        screen: AuthLoadingStack,
        navigationOptions: {
            header: null
        }
    },
    Auth: {
        screen: LoginStack,
        navigationOptions: {
            header: null
        }
    },
    Anonymous: {
        screen: AnonymousStack,
        navigationOptions: {
            header: null
        }
    },
    User: {
        screen: UserStack,
        navigationOptions: {
            header: null
        }
    }
})

export default createAppContainer(Root);

class TabBarIcon extends Component {
    render() {
        return (
            <Image source={this.props.focused ? this.props.focusedIcon : this.props.icon} style={{ width: 25, height: 25, alignSelf: 'center' }}></Image>
        )
    }
}