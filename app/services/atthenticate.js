import { getToken } from "./storage";

export async function authenticate(){
    var token = await getToken();
    if(token==null || token==""){
        return false;
    }else{
        return true;
    }
}