import { types } from "mobx-state-tree";
import { getData } from "../services/fetch";
import { API_URL } from "../components/common";

const post = types.model('Post', {
    id: types.string,
    des: types.string,
    title: types.string,
    price: types.string,
    picserver: types.string,
    picpath: types.string,
    picname: types.string,
    count: types.number,
    totalcount: types.number,
    condition: types.number,
    name: types.string,
    phone: types.string
})

const PostStore = types.model({
    Posts: types.array(post),
    BrandId: types.string,
    BrandName:types.string,
    Context: types.string,
    PhoneId:types.string,
    Minimum:types.number,
    Maximum:types.number
})
    .actions(self => ({
        async load() {
            var response = await getData(API_URL + 'basic/search?brandId=' + self.BrandId + '&phoneId='+ self.PhoneId + '&min=' + self.Minimum + '&max=' + self.Maximum  + '&text=' + self.Context);
            if (response.ok) {
                self.updatePosts(response.data);
            }else{
                console.log(response.data);
            }
        },

        updatePosts(postsToUpdate) {
            self.Posts = postsToUpdate;
        },

        updateBrandId(brandIdToUpdate, brandNameToUpdate){
            self.BrandId = brandIdToUpdate;
            self.BrandName = brandNameToUpdate;
        },

        updateFilter(brandId, phoneId, min, max){
            self.BrandId = brandId;
            self.PhoneId = phoneId,
            self.Minimum = parseInt(min),
            self.Maximum = parseInt(max)
        },

        updateContext(context){
            self.Context = context;
        }
    }))
    .create({
        Posts: [],
        BrandId: '',
        BrandName:'',
        Context: '',
        PhoneId:'',
        Minimum:0,
        Maximum:0
    })

export default PostStore;   