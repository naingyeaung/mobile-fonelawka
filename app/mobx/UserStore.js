import { types } from "mobx-state-tree";
import { getData, postData, putData } from "../services/fetch";
import { API_URL } from "../components/common";
import AsyncStorage from "@react-native-community/async-storage";


const phone = types.model('phone', {
    id: types.string,
    phoneNumber: types.maybeNull(types.string),
})

const UserStore = types.model({
    User: types.model({
        id: types.string,
        name: types.string,
        description: types.maybeNull(types.string),
        homeandStreet: types.maybeNull(types.string),
        avatar: types.string
    }),
    Phones: types.array(phone)
})
    .actions(self => ({
        async load() {
            var role = await AsyncStorage.getItem('role')
            var response = role != 'user' ? await getData(API_URL + 'api/shops/getShopDetailInfo') : await getData(API_URL + 'api/users/getUserDetailInfo');
            if (response.ok) {
                console.log(response.data)
                self.updateUser(response.data);
            } else {
                console.log(response.data)
            }
        },

        async editUser() {
            var role = await AsyncStorage.getItem('role')
            var user = self.User;
            const model = {
                id: user.id,
                name: user.name,
                homeandStreet: user.homeandStreet,
            };
            console.log(model);
            var response = role != 'user' ? await putData(API_URL + 'api/shops/putShops/' + user.id, model) : await putData(API_URL + 'api/users/putUsers/' + user.id, model);
            if (response.ok) {
                console.log(response.data);
            } else {
                console.log(response.data);
            }

            if (self.Phones.length >= 2) {
                var fpModel = {
                    phoneNumber : self.Phones[0].phoneNumber
                }
                var fpResponse = await putData(API_URL + 'api/users/updateUserFirstPhone/' + self.Phones[0].id, fpModel);
                console.log(API_URL + 'api/users/updateUserFirstPhone/' + self.Phones[0].id)
                if (fpResponse.ok) {
                    console.log(fpResponse.data);
                } else {
                    console.log(fpResponse.data);
                }

                var fsModel = {
                    phoneNumber : self.Phones[1].phoneNumber
                }
                var fsResponse = await putData(API_URL + 'api/users/updateUserSecondPhone/' + self.Phones[1].id, fsModel);
                if (fsResponse.ok) {
                    console.log(fsResponse.data);
                } else {
                    console.log(fsResponse.data);
                }
            }
        },

        updateUser(userToUpdate) {
            self.User = userToUpdate.user;
            self.Phones = userToUpdate.phones;
        },

        updatePhone(index, number) {
            self.Phones[index].phoneNumber = number;
        },

        updateName(name) {
            self.User.name = name;
        },

        updateAddress(address) {
            self.User.homeandStreet = address
        }
    }))
    .create({
        User: { id: '', name: '', description: '', homeandStreet: '', avatar: '' },
        Phones: []
    })

export default UserStore;   